import numpy as np
import glob
import os
import sys
from scipy.io import readsav
from collections import OrderedDict
import struct


# Functions used by the wrapper
def BODHAINE(wv0, pz1, pz2):

    """
    wv0 = wavelength (in microns) --- can be an array or a value
    pz1 = Pressure of lower layer (hPa)
    pz2 = Pressure of upper layer (hPa; pz1 > pz2)

    example: calculate Rayleigh optical depth between 37 km (~4 hPa) and sea level (1000 hPa) at 0.5 microns:
    in Python program:
        result=bodhaine(0.5,1000,4)

    Note: If you input an array of wavelengths, the result will also be an
          array corresponding to the Rayleigh optical depth at these wavelengths.
    """

    num = 1.0455996 - 341.29061*wv0**(-2.0) - 0.90230850*wv0**2.0
    den = 1.0 + 0.0027059889*wv0**(-2.0) - 85.968563*wv0**2.0
    tauray = 0.00210966*(num/den)*(pz1-pz2)/1013.25
    return tauray





def MCARaTS_RUN(mca_obj):

<<<<<<< HEAD
    command_string = '%s %d %d %s %s > %s' % (mca_obj.mca_exe, mca_obj.Np, mca_obj.target, mca_obj.fname_inp, mca_obj.fname_out, mca_obj.fname_log)
    os.system(command_string)

=======
    # run MCARaTS
    command_string = '%s %d %d %s %s > %s' % (mca_obj.mca_exe, mca_obj.Np, mca_obj.target, mca_obj.fname_inp, mca_obj.fname_out, mca_obj.fname_log)
    os.system(command_string)
>>>>>>> 126af9f47639e6b60eb95c4878669cafa90f9ffa





def MCARaTS_RUN_MP(mca_objs, Ncpu=4):

    import multiprocessing as mp
    pool = mp.Pool(processes=Ncpu)
    pool.outputs = pool.map(MCARaTS_RUN, mca_objs)
    pool.close()
    pool.join()





def MCARaTS_INPUT(input_fname, input_dict, verbose=True, comment=True):

    #+ MCARaTS initialization
    mcarWld_nml_init = OrderedDict([
                         ('Wld_mverb'  , None),
                         ('Wld_jseed'  , None),
                         ('Wld_mbswap' , None),
                         ('Wld_mtarget', None),
                         ('Wld_moptim' , None),
                         ('Wld_njob'   , None)])

    mcarSca_nml_init = OrderedDict([
                       ('Sca_inpfile', None),
                       ('Sca_npf'    , None),
                       ('Sca_nangi'  , None),
                       ('Sca_nskip'  , None),
                       ('Sca_ndfl'   , None),
                       ('Sca_nchi'   , None),
                       ('Sca_ntg'    , None),
                       ('Sca_qtfmax' , None)])

    mcarAtm_nml_init = OrderedDict([
                       ('Atm_inpfile', None),
                       ('Atm_nx'     , None),
                       ('Atm_ny'     , None),
                       ('Atm_nz'     , None),
                       ('Atm_iz3l'   , None),
                       ('Atm_nz3'    , None),
                       ('Atm_nwl'    , None),
                       ('Atm_np1d'   , None),
                       ('Atm_np3d'   , None),
                       ('Atm_nqlay'  , None),
                       ('Atm_iipfd1d', None),
                       ('Atm_iipfd3d', None)])

    mcarSfc_nml_init = OrderedDict([
                       ('Sfc_inpfile', None),
                       ('Sfc_mbrdf'  , None),
                       ('Sfc_nxb'    , None),
                       ('Sfc_nyb'    , None),
                       ('Sfc_nsco'   , None),
                       ('Sfc_nsuz'   , None)])

    mcarSrc_nml_init = OrderedDict([
                       ('Src_nsrc', None)])

    mcarFlx_nml_init = OrderedDict([
                       ('Flx_mflx'   , None),
                       ('Flx_mhrt'   , None),
                       ('Flx_nxf'    , None),
                       ('Flx_nyf'    , None),
                       ('Flx_diff0'  , None),
                       ('Flx_diff1'  , None),
                       ('Flx_cf_dtau', None)])

    mcarRad_nml_init = OrderedDict([
                       ('Rad_mrkind', None),
                       ('Rad_mpmap' , None),
                       ('Rad_mplen' , None),
                       ('Rad_nrad'  , None),
                       ('Rad_nxr'   , None),
                       ('Rad_nyr'   , None),
                       ('Rad_nwf'   , None),
                       ('Rad_ntp'   , None),
                       ('Rad_tpmin' , None),
                       ('Rad_tpmax' , None)])

    mcarVis_nml_init = OrderedDict([
                       ('Vis_mrend'  , None),
                       ('Vis_epserr' , None),
                       ('Vis_fpsmth' , None),
                       ('Vis_fatten' , None),
                       ('Vis_nqhem'  , None)])

    mcarPho_nml_init = OrderedDict([
                       ('Pho_iso_SS' , None),
                       ('Pho_iso_tru', None),
                       ('Pho_iso_max', None),
                       ('Pho_wmin'   , None),
                       ('Pho_wmax'   , None),
                       ('Pho_wfac'   , None),
                       ('Pho_pfpeak' , None)])
    #-

    #+ MCARaTS job
    mcarWld_nml_job  = OrderedDict([
                       ('Wld_nplcf', None)])

    mcarAtm_nml_job  = OrderedDict([
                       ('Atm_idread'    , None),
                       ('Atm_dx'        , None),
                       ('Atm_dy'        , None),
                       ('Atm_zgrd0'     , None),
                       ('Atm_ext1d'     , None),
                       ('Atm_omg1d'     , None),
                       ('Atm_apf1d'     , None),
                       ('Atm_abs1d'     , None),
                       ('Atm_fext1d'    , None),
                       ('Atm_fext3d'    , None),
                       ('Atm_fabs1d'    , None),
                       ('Atm_fabs3d'    , None),
                       ('Atm_mcs_rat'   , None),
                       ('Atm_mcs_frc'   , None),
                       ('Atm_mcs_dtauz' , None),
                       ('Atm_mcs_dtauxy', None)])

    mcarSfc_nml_job  = OrderedDict([
                       ('Sfc_idread', None),
                       ('Sfc_mtype' , None),
                       ('Sfc_param' , None),
                       ('Sfc_nudsm' , None),
                       ('Sfc_nurpv' , None),
                       ('Sfc_nulsrt', None),
                       ('Sfc_nqpot' , None),
                       ('Sfc_rrmax' , None),
                       ('Sfc_rrexp' , None)])

    mcarSrc_nml_job  = OrderedDict([
                       ('Src_mphi', None),
                       ('Src_flx' , None),
                       ('Src_qmax', None),
                       ('Src_the' , None),
                       ('Src_phi' , None)])

    mcarRad_nml_job  = OrderedDict([
                       ('Rad_mrproj' , None),
                       ('Rad_difr0'  , None),
                       ('Rad_difr1'  , None),
                       ('Rad_zetamin', None),
                       ('Rad_npwrn'  , None),
                       ('Rad_npwrf'  , None),
                       ('Rad_cf_dmax', None),
                       ('Rad_cf_taus', None),
                       ('Rad_wfunc0' , None),
                       ('Rad_rmin0'  , None),
                       ('Rad_rmid0'  , None),
                       ('Rad_rmax0'  , None),
                       ('Rad_phi'    , None),
                       ('Rad_the'    , None),
                       ('Rad_psi'    , None),
                       ('Rad_umax'   , None),
                       ('Rad_vmax'   , None),
                       ('Rad_qmax'   , None),
                       ('Rad_xpos'   , None),
                       ('Rad_ypos'   , None),
                       ('Rad_zloc'   , None),
                       ('Rad_apsize' , None),
                       ('Rad_zref'   , None)])

    #-

    #+ MCARaTS input parameter descriptions
    mcarats_var_details = OrderedDict([
    ('KNDFL' , 'default=100'),
    ('KNP1D' , 'default=100'),
    ('KNP3D' , 'default=100'),
    ('KNWL'  , 'default=3000'),
    ('KNZ'   , 'default=3000'),
    ('KNSRC' , 'default=3000'),
    ('KNWF'  , 'default=30'),

    ('Wld_mverb'  , 'default=0 : verbose mode (0=quiet, 1=yes, 2=more, 3=most)'),
    ('Wld_jseed'  , 'default=0 : seed for random number generator (0 for automatic)'),
    ('Wld_mbswap' , 'default=0 : flag for byte swapping for binary input files (0=no, 1=yes)'),
    ('Wld_mtarget', 'default=1 : flag for target quantities\n\
      = 1 : fluxes and heating rates, calculated by MC\n\
      = 2 : radiances, calculated by MC\n\
      = 3 : quasi-radiances (or some signals), calculated by volume rendering'),
    ('Wld_moptim' , 'default=2 : flag for optimization of calculation techniques\n\
      = -2 : no tuning (use default optimization)\n\
      = -1 : no optimization (deactivate all optimization)\n\
      =  0 : unbiased optimization (deactivate all biasing optimizations)\n\
      =  1 : conservative optimizations (possibly for smaller biases)\n\
      =  2 : standard optimizations (recommended)\n\
      =  3 : quick-and-dirty optimizations (for speed when small biases are acceptable)'),
    ('Wld_njob'   , 'default=1 : # of jobs in a single experiment'),

    ('Sca_inpfile' , 'default(KNDFL)=\' \' : file names for scattering phase functions'),
    ('Sca_npf'     , 'default(KNDFL)=0 : # of tabulated phase functions in each file'),
    ('Sca_nangi'   , 'default(KNDFL)=100 : # of angles in each file'),
    ('Sca_nskip'   , 'default(KNDFL)=0 : # of data record lines to be skipped'),
    ('Sca_ndfl'    , 'default=0 : # of scattering phase function data files'),
    ('Sca_nchi'    , 'default=4 : # of orders for truncation approximation (>= 2)'),
    ('Sca_ntg'     , 'default=20000 : # of table grids for angles & probabilities'),
    ('Sca_qtfmax'  , 'default=20.0  : geometrical truncation angle (deg.)'),

    ('Atm_inpfile' , 'default=\' \' : file name for input of 3D otpical properties'),
    ('Atm_nx'      , 'default=1 : # of X grid points'),
    ('Atm_ny'      , 'default=1 : # of Y grid points'),
    ('Atm_nz'      , 'default=1 : # of Z grid points'),
    ('Atm_iz3l'    , 'default=1 : layer index for the lowest 3-D layer'),
    ('Atm_nz3'     , 'default=0 : # of 3-D inhomogeneous layers'),
    ('Atm_nwl'     , 'default=1 : # of wavelengths'),
    ('Atm_np1d'    , 'default=1 : # of scattering components in 1D medium'),
    ('Atm_np3d'    , 'default=1 : # of scattering components in 3D medium'),
    ('Atm_nqlay'   , 'default=10 : # of Gaussian quadrature points per layer'),
    ('Atm_iipfd1d' , 'default(KNP1D)=1 : indices for phase function data files for 1D medium'),
    ('Atm_iipfd3d' , 'default(KNP3D)=1 : indices for phase function data files for 3D medium'),

    ('Sfc_inpfile' , 'default=\' \' : file name for input of surface properties'),
    ('Sfc_mbrdf'   , 'default(4)=(1, 1, 1, 1) : flags of on/off status for BRDF models'),
    ('Sfc_nxb'     , 'default=1 : # of X grid points'),
    ('Sfc_nyb'     , 'default=1 : # of Y grid points'),
    ('Sfc_nsco'    , 'default=60 : # of uz0 for coefficient table'),
    ('Sfc_nsuz'    , 'default=200 : # of uz0 grids for albedo LUTs'),
    ('Src_nsrc'    , 'default=1 : # of sources'),

    ('Flx_mflx'    , 'default=1 : flag for flux density calculations (0=off, 1=on)'),
    ('Flx_mhrt'    , 'default=1 : flag for heating rate calculations (0=off, 1=on)'),
    ('Flx_nxf'     , 'default=1 : # of cells along X'),
    ('Flx_nyf'     , 'default=1 : # of cells along Y'),
    ('Flx_diff0'   , 'default=1.0 : numerical diffusion parameter'),
    ('Flx_diff1'   , 'default=0.01 : numerical diffusion parameter'),
    ('Flx_cf_dtau' , 'default=0.2 : Delta_tau, layer optical thickness for collision forcing'),

    ('Rad_mrkind'  , 'default=2 : a kind of radiance\n\
      = 0 : no radiance calculation\n\
      = 1 : 1st kind, local radiance averaged over solid angle\n\
      = 2 : 2nd kind, averaged over pixel (horizontal cross section of atmospheric column)'),
    ('Rad_mpmap'   , 'default=1 : method for pixel mapping\n\
      = 1 : polar       (U = theta * cos(phi), V = theta * sin(phi))\n\
      = 2 : rectangular (U = theta, V = phi)'),
    ('Rad_mplen'   , 'default=0 : method of calculation of pathlength statistics\n\
      = 0 : (npr = 0)  no calculation of pathlength statistics\n\
      = 1 : (npr = nz) layer-by-layer pathlength distribution\n\
      = 2 : (npr = nwf) average pathlengths with weighting functions\n\
      = 3 : (npr = ntp) histogram of total, integrated pathelength'),
    ('Rad_nrad'    , 'default=0 : # of radiances'),
    ('Rad_nxr'     , 'default=1 : # of X grid points'),
    ('Rad_nyr'     , 'default=1 : # of Y grid points'),
    ('Rad_nwf'     , 'default=1 : # of weighting functions'),
    ('Rad_ntp'     , 'default=100 : # of total pathlength bins'),
    ('Rad_tpmin'   , 'default=0.0 : min of total pathlength'),
    ('Rad_tpmax'   , 'default=1.0e5 : max of total pathlength'),

    ('Vis_mrend'   , 'default=2 : method for rendering (0/1/2/3/4)\n\
      = 0 : integration of density along the ray (e.g. for calculating optical thickness)\n\
      = 1 : as 1, but with attenuation (assuming that the source function is uniformly 1)\n\
      = 2 : RTE-based solver, assuming horizontally-uniform source functions\n\
      = 3 : as 2, but taking into accout 3-D distribution of 1st-order source functions (J1)\n\
      = 4 : as 3, but with TMS correction for anisotropic scattering'),
    ('Vis_epserr'  , 'default=1.0e-4 : convergence criterion for multiple scattering components'),
    ('Vis_fpsmth'  , 'default=0.5 : phase function smoothing fraction for relaxed TMS correction'),
    ('Vis_fatten'  , 'default=1.0 : attenuation factor (1 for physics-based rendering)'),
    ('Vis_nqhem'   , 'default=1 : # of Gaussian quadrature points in a hemisphere'),

    ('Pho_iso_SS'  , 'default=1 : scattering order at which 1-D transfer begins'),
    ('Pho_iso_tru' , 'default=0 : truncation approximations are used after this scattering order'),
    ('Pho_iso_max' , 'default=1000000 : max scattering order for which radiation is sampled'),
    ('Pho_wmin'    , 'default=0.2 : min photon weight'),
    ('Pho_wmax'    , 'default=3.0 : max photon weight'),
    ('Pho_wfac'    , 'default=1.0 : factor for ideal photon weight'),
    ('Pho_pfpeak'  , 'default=30.0 : phase function peak threshold'),

    ('Wld_nplcf'   , 'default=0 : dummy variable (will be removed in the future)'),

    ('Atm_idread'    , 'default=1 : location of data to be read'),
    ('Atm_dx'        , 'default=1.0e4 : X size of cell'),
    ('Atm_dy'        , 'default=1.0e4 : Y size of cell'),
    ('Atm_zgrd0'     , 'default(KNZ+1)=10.0 : Z (height) at layer interfaces'),
    ('Atm_ext1d'     , 'default(KNZ,KNP1D) : extinction coefficients'),
    ('Atm_omg1d'     , 'default(KNZ,KNP1D) : single scattering albedos'),
    ('Atm_apf1d'     , 'default(KNZ,KNP1D) : phase function specification parameters'),
    ('Atm_abs1d'     , 'default(KNZ,KNWL) : absorption coefficients'),
    ('Atm_fext1d'    , 'default(KNP1D)=1.0 : scaling factor for Atm_ext1d'),
    ('Atm_fext3d'    , 'default(KNP3D)=1.0 : scaling factor for Atm_ext3d'),
    ('Atm_fabs1d'    , 'default=1.0 : scaling factor for Atm_abs1d'),
    ('Atm_fabs3d'    , 'default=1.0 : scaling factor for Atm_abs3d'),
    ('Atm_mcs_rat'   , 'default=1.5 : threshold for max/mean extinction coefficient ratio'),
    ('Atm_mcs_frc'   , 'default=0.8 : threshold for fraction of super-voxels good for MCS'),
    ('Atm_mcs_dtauz' , 'default=2.0 : Delta_tau_z,  threshold for super-voxel optical thickness'),
    ('Atm_mcs_dtauxy', 'default=4.0 : Delta_tau_xy, threshold for super-voxel optical thickness'),

    ('Sfc_NPAR'  , 'default=5'),
    ('Sfc_idread', 'default=1 : index of data to be read'),
    ('Sfc_mtype' , 'default=1 : surface BRDF type'),
    ('Sfc_param' , 'default(Sfc_NPAR) : BRDF parameters\n\
      = (1.0, 0.0, 0.0, 0.0, 0.0)'),
    ('Sfc_nudsm' , 'default=14 : # of table grid points for DSM model'),
    ('Sfc_nurpv' , 'default=8 : # of table grid points for RPV model'),
    ('Sfc_nulsrt', 'default=14 : # of table grid points for LSRT model'),
    ('Sfc_nqpot' , 'default=24 : # of quadrature points for preprocess'),
    ('Sfc_rrmax' , 'default=5.0 : max factor for relative BRDF used for random directions'),
    ('Sfc_rrexp' , 'default=0.5 : scaling exponent for relative BRDF used for random directions'),

    ('Src_mphi'  , 'default(KNSRC)=0 : flag for random azimuth'),
    ('Src_flx'   , 'default(KNSRC)=1.0 : source flux density'),
    ('Src_qmax'  , 'default(KNSRC)=0.0 : full cone angle'),
    ('Src_the'   , 'default(KNSRC)=120.0 : zenith angle'),
    ('Src_phi'   , 'default(KNSRC)=0.0 : azimuth angle'),

    ('Rad_mrproj'    , 'default=0 : flag for angular weighting (for mrkind = 1 or 3)\n\
      = 0 : w = 1, results are radiances simply averaged over solid angle\n\
      = 1 : w = cosQ for Q = angle from the camera center direction, results are weighted average\n\
      Eamples: When FOV = hemisphere (nxr = 1, nyr = 1, mpmap = 2, umax = 90 deg.),\n\
      mrproj = 0 for hemispherical-mean radiance (actinic flux density)\n\
      mrproj = 1 for irradiance (flux density)'),
    ('Rad_difr0'     , 'default=10.0 : numerical diffusion parameter'),
    ('Rad_difr1'     , 'default=0.01 : numerical diffusion parameter'),
    ('Rad_zetamin'   , 'default=0.01 : threshold for radiance contribution function'),
    ('Rad_npwrn'     , 'default=1 : power exponent for scaling of near-field radiance contribution'),
    ('Rad_npwrf'     , 'default=1 : power exponent for scaling of  far-field radiance contribution'),
    ('Rad_cf_dmax'   , 'default=10.0 : Delta_Tau_s,max, max layer optical thickness for CF scattering'),
    ('Rad_cf_taus'   , 'default=5.0 : Tau_s,cf, scattering optical thickness for CF'),
    ('Rad_wfunc0'    , 'default(KNZ,KNWF) : weighting functions used when Rad_mplen=2'),
    ('Rad_rmin0'     , 'default(KNRAD)=1.0e-17 : min distance (from camera)'),
    ('Rad_rmid0'     , 'default(KNRAD)=1.0e3 : moderate distance (from camera)'),
    ('Rad_rmax0'     , 'default(KNRAD)=1.0e17 : max distance (from camera)'),
    ('Rad_phi'       , 'default(KNRAD)=0.0 : phi,   angle around Z0'),
    ('Rad_the'       , 'default(KNRAD)=0.0 : theta, angle around Y1'),
    ('Rad_psi'       , 'default(KNRAD)=0.0 : psi,   angle around Z2\n\
      Camera coordinates : Z-Y-Z, three rotations\n\
      1: rotation about Z0 (original Z-axis in world coordinates) by phi\n\
      2: rotation about Y1 (Y-axis in (X1,Y1,Z1) coordinates) by theta\n\
      3: rotation about Z2 (Z-axis in (X2,Y2,Z2) coordinates) by psi'),
    ('Rad_umax'      , 'default(KNRAD)=180.0 : max angle along U-direction'),
    ('Rad_vmax'      , 'default(KNRAD)=180.0 : max angle along V-direction'),
    ('Rad_qmax'      , 'default(KNRAD)=180.0 : max angle of FOV cone'),
    ('Rad_xpos'      , 'default(KNRAD)=0.5 : X relative position'),
    ('Rad_ypos'      , 'default(KNRAD)=0.5 : Y relative position'),
    ('Rad_zloc'      , 'default(KNRAD)=0.0 : Z location'),
    ('Rad_apsize'    , 'default(KNRAD)=0.0 : aperture size'),
    ('Rad_zref'      , 'default(KNRAD)=0.0 : Z location of the reference level height')])
    #-

    mcarats_nml = OrderedDict([
                   ('mcarWld_nml_init', mcarWld_nml_init),
                   ('mcarSca_nml_init', mcarSca_nml_init),
                   ('mcarAtm_nml_init', mcarAtm_nml_init),
                   ('mcarSfc_nml_init', mcarSfc_nml_init),
                   ('mcarSrc_nml_init', mcarSrc_nml_init),
                   ('mcarFlx_nml_init', mcarFlx_nml_init),
                   ('mcarRad_nml_init', mcarRad_nml_init),
                   ('mcarVis_nml_init', mcarVis_nml_init),
                   ('mcarPho_nml_init', mcarPho_nml_init),
                   ('mcarWld_nml_job' , mcarWld_nml_job) ,
                   ('mcarAtm_nml_job' , mcarAtm_nml_job) ,
                   ('mcarSfc_nml_job' , mcarSfc_nml_job) ,
                   ('mcarSrc_nml_job' , mcarSrc_nml_job) ,
                   ('mcarRad_nml_job' , mcarRad_nml_job)])

    nml_ordered_keys_full = []
    nml_ordered_item_full = []
    for nml_key in mcarats_nml.keys():
        for var_key in mcarats_nml[nml_key].keys():
            nml_ordered_keys_full.append(var_key)
            nml_ordered_item_full.append(nml_key)

    # check input variables
    #
    # if input variable is in the full dictionary keys, assign input data to
    # the correspond variable
    #
    # if not
    #   1. if is a typo in input variables, exit with error message
    #   2. if in array-like way, e.g., 'Atm_ext1d(1:, 1)' is similar to
    #      'Atm_ext1d', update dictionary with new variable and assign data
    #      to the updated variable.
    for key in input_dict.keys():
        if key not in nml_ordered_keys_full:
            if '(' in key and ')' in key:
                ee          = key.index('(')
                key_ori     = key[:ee]
                index_ori   = nml_ordered_keys_full.index(key_ori)

                key_more = [xx for xx in nml_ordered_keys_full if key_ori in xx and '(' in xx]

                if len(key_more) >= 1:
                    key0     = key_more[-1]
                    index    = nml_ordered_keys_full.index(key0)
                else:
                    index = index_ori

                nml_ordered_keys_full.insert(index+1, key)
                nml_ordered_item_full.insert(index+1, nml_ordered_item_full[index])
                mcarats_nml[nml_ordered_item_full[index]][key] = input_dict[key]
                if comment:
                    mcarats_var_details[key] = mcarats_var_details[key_ori]
            else:
                exit('Error [MCARaTS]: please check input variable \'%s\'.' % key)
        else:
            index   = nml_ordered_keys_full.index(key)
            mcarats_nml[nml_ordered_item_full[index]][key] = input_dict[key]

    # create a full dictionary to link variable back to namelist section
    # examples:
    #   'Wld_mverb': 'mcarWld_nml_init'
    #   'Atm_zgrd0': 'mcarAtm_nml_job'
    nml_full = OrderedDict(zip(nml_ordered_keys_full, nml_ordered_item_full))

    # creating input file for MCARaTS
    f = open(input_fname, 'w')

    for nml_key in mcarats_nml.keys():
        f.write('&%s\n' % nml_key)

        vars_key = [xx for xx in nml_full.keys() if nml_full[xx]==nml_key]
        for var_key in vars_key:
            var = mcarats_nml[nml_key][var_key]
            if var is not None:

                if isinstance(var, (int, float)):
                    f.write(' %-15s = %-g\n' % (var_key, var))
                elif isinstance(var, str):
                    if '*' in var:
                        f.write(' %-15s = %s\n' % (var_key, var))
                    else:
                        f.write(' %-15s = \'%s\'\n' % (var_key, var))
                elif isinstance(var, np.ndarray):
                    var_str = np.array2string(var, separator=',   ', max_line_width=80, suppress_small=False)
                    if len(var_str) <= 80:
                        f.write(' %-15s = %s\n' % (var_key, var_str[1:-1].rstrip()))
                    else:
                        f.write(' %-15s =\n' % var_key)
                        f.write(' %s\n' % var_str[1:-1].rstrip())
                    #f.write(' %-15s = %s\n' % (var_key, var_str[1:-1].rstrip()))
                else:
                    exit('Error [MCARaTS_INPUT]: only types of int, float, str, ndarray are supported.')

                if comment:
                    var_detail = mcarats_var_details[var_key]
                    f.write(' !------------------------------- This is a comment for above parameter -------------------------------------\n')
                    if '\n' in var_detail:
                        lines = var_detail.split('\n')
                        for line in lines:
                            f.write(' !----> %s\n' % line)
                    else:
                        f.write(' !----> %s\n' % mcarats_var_details[var_key])
                    f.write(' !-----------------------------------------------------------------------------------------------------------\n')
                    f.write('\n')

        f.write('/\n')

    f.close()






def MAKE_ATM(fname_in, fname_out, nwl, nx=100, ny=100):

    Nbyte = os.path.getsize(fname_in)
    Ndata = Nbyte / 4

    atm_abs = np.zeros(nx*ny*nwl, dtype=np.float32)

    f = open(fname_in, 'rb')
    rec_bin = f.read(Nbyte)
    data    = struct.unpack('<%sf' % str(Ndata), rec_bin)
    atm_ext = np.array(data[3*nx*ny:4*nx*ny])
    atm_omg = np.array(data[4*nx*ny:5*nx*ny])
    atm_apf = np.array(data[5*nx*ny:6*nx*ny])
    f.close()

    f = open(fname_out, 'wb')
    f.write(struct.pack('<%sf' % str(nx*ny*nwl), *atm_abs))
    f.write(struct.pack('<%sf' % str(nx*ny)    , *atm_ext))
    f.write(struct.pack('<%sf' % str(nx*ny)    , *atm_omg))
    f.write(struct.pack('<%sf' % str(nx*ny)    , *atm_apf))
    f.close()





def MAKE_SFC(fname_in, fname_out, nx=12, ny=17):

    Nbyte = os.path.getsize(fname_in)
    Ndata = Nbyte / 4

    f = open(fname_in, 'rb')
    rec_bin = f.read(Nbyte)
    data    = struct.unpack('<%sf%si%sf' % (str(nx*ny), str(nx*ny), str(nx*ny)), rec_bin)
    sfc_jsfc2d = np.array(data[1*nx*ny:2*nx*ny])
    sfc_psfc2d = np.array(data[2*nx*ny:3*nx*ny])
    f.close()

    f = open(fname_out, 'wb')
    f.write(struct.pack('<%si' % str(nx*ny), *sfc_jsfc2d))
    f.write(struct.pack('<%sf' % str(nx*ny), *sfc_psfc2d))
    f.close()





def OCO_TEST(gi):

    wvl      = 760.0
    fname_atm_1d = '/data/hong/work/01_mcarats/pro/02_driver/data/atm_cld_1d.760.idl'
    fname_atm_3d = '/data/hong/work/01_mcarats/pro/02_driver/data/atm_abs.idl'
    f_atm_1d = readsav(fname_atm_1d)
    f_atm_3d = readsav(fname_atm_3d)

    Nlayer = f_atm_3d.atm_dz.size
    Ng     = f_atm_3d.absgn[gi].item()

    fname_inp_atm = '/data/hong/work/01_mcarats/pro/02_driver/data/atm_cld_3d.760.bin'
    fname_out_atm = '%s.%2.2d.bin' % (fname_inp_atm.split('/')[-1][:-4], gi)
    MAKE_ATM(fname_inp_atm, fname_out_atm, Ng)
    fname_inp_sfc = '/data/hong/work/01_mcarats/pro/02_driver/data/sfc_2d.760.bin'
    fname_out_sfc = '%s.%2.2d.bin' % (fname_inp_sfc.split('/')[-1][:-4], gi)
    MAKE_SFC(fname_inp_sfc, fname_out_sfc)

    input_dict = OrderedDict()

    input_dict['Wld_mtarget'] = 2
    input_dict['Wld_mverb']   = 0
    input_dict['Wld_mbswap']  = 0
    input_dict['Wld_moptim']  = 2
    input_dict['Wld_njob']    = 1

    input_dict['Sca_inpfile(1)'] = 'pha_mie.760.pha'
    input_dict['Sca_nangi(1)']   = 498
    input_dict['Sca_npf(1)']     = 25
    input_dict['Sca_ndfl']       = 1

    input_dict['Atm_inpfile'] = fname_out_atm
    input_dict['Atm_np1d']    = 2
    input_dict['Atm_np3d']    = 1
    input_dict['Atm_nx']      = 100
    input_dict['Atm_ny']      = 100
    input_dict['Atm_nz']      = 29
    input_dict['Atm_iz3l']    = 5
    input_dict['Atm_nz3']     = 1
    input_dict['Atm_nwl']     = Ng
    #input_dict['Atm_nwl']     = 1

    input_dict['Sfc_inpfile'] = fname_out_sfc
    input_dict['Sfc_nxb']     = 12
    input_dict['Sfc_nyb']     = 17
    input_dict['Sfc_mbrdf(1)']= 1
    input_dict['Sfc_mbrdf(2)']= 0
    input_dict['Sfc_mbrdf(3)']= 0
    input_dict['Sfc_mbrdf(4)']= 0

    input_dict['Src_nsrc']    = 1

    input_dict['Rad_mrkind']  = 2
    input_dict['Rad_mplen']   = 0
    input_dict['Rad_mpmap']   = 1
    input_dict['Rad_nrad']    = 1
    input_dict['Rad_nxr']     = 100
    input_dict['Rad_nyr']     = 100

    input_dict['Atm_idread']  = 1
    input_dict['Atm_dx']      = 500.0
    input_dict['Atm_dy']      = 500.0
    input_dict['Atm_zgrd0']   = f_atm_3d.atm_zgrd
    input_dict['Atm_ext1d(1:, 1)'] = BODHAINE(wvl*0.001, f_atm_3d.atm_pi[:-1], f_atm_3d.atm_pi[1:]) / f_atm_3d.atm_dz
    #input_dict['Atm_ext1d(1:, 1)'] = '29*0.0'
    input_dict['Atm_omg1d(1:, 1)'] = np.repeat(1 , Nlayer)
    input_dict['Atm_apf1d(1:, 1)'] = np.repeat(-1, Nlayer)
    input_dict['Atm_ext1d(1:, 2)'] = f_atm_1d.atm_ext_1d
    #input_dict['Atm_ext1d(1:, 2)'] = '29*0.0'
    input_dict['Atm_omg1d(1:, 2)'] = f_atm_1d.atm_omg_1d
    input_dict['Atm_apf1d(1:, 2)'] = f_atm_1d.atm_apf_1d
    for i in xrange(Ng):
        vname = 'Atm_abs1d(1:, %d)' % (i+1)
        input_dict[vname] = f_atm_3d.absgl[i, gi, :]*0.001
    #input_dict['Atm_abs1d(1:, 1)'] = f_atm_3d.absgl[0, gi, :]*0.001

    #input_dict['Sfc_idread']  = 1
    #input_dict['Sfc_param(1)']  = 0.0

    input_dict['Src_flx']     = 1.0
    input_dict['Src_qmax']    = 0.533133
    input_dict['Src_mphi']    = 0
    input_dict['Src_the']     = 145.0
    input_dict['Src_phi']     = 53.0

    input_dict['Rad_the']     = 146.0
    #input_dict['Rad_the']     = 180.0
    input_dict['Rad_phi']     = -47.0
    input_dict['Rad_psi']     = 270.0
    input_dict['Rad_zloc']    = 705000.0
    #input_dict['Rad_zloc']    = 0.0
    input_dict['Rad_xpos']    = 0.5
    input_dict['Rad_ypos']    = 0.5
    input_dict['Rad_difr0']   = 7.5
    input_dict['Rad_difr1']   = 0.0025
    #input_dict['Rad_difr0']   = 0.0
    #input_dict['Rad_difr1']   = 0.0
    input_dict['Rad_rmin0']   = 10000.0
    input_dict['Rad_rmid0']   = 30000.0
    input_dict['Rad_rmax0']   = 60000.0
    input_dict['Rad_umax']    = 50
    input_dict['Rad_vmax']    = 50
    input_dict['Rad_qmax']    = 80
    input_dict['Rad_zref']    = 0

    MCARaTS_INPUT('new_%2.2d.inp' % gi, input_dict, comment=False)








if __name__ == '__main__':
    for gi in range(11):
        OCO_TEST(gi)
    #fname = 'hehe.2.out'
    #MCARaTS_OUTPUT(fname)
    #TEST_ATM_ONLY()
    #TEST_1D_AER_PHA_N()
    #TEST_1D_AER_PHA_Y()
    #TEST_1D_CLD()
    #TEST_ATM_ONLY()
    #TEST_CASE_ci0045()
