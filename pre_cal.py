import numpy as np

def GET_CH4_MIX(h):
    #{{{
    ch4h   = np.array([ 0.000000,      0.100000,      0.200000,      0.300000, \
                        0.400000,      0.500000,      1.000000,      2.000000, \
                        3.000000,      4.000000,      5.000000,      6.000000, \
                        7.000000,      8.000000,      9.000000,     10.000000, \
                       11.000000,     12.000000,     13.000000,     14.000000, \
                       15.000000,     16.000000,     17.000000,     18.000000, \
                       19.000000,     20.000000,     21.000000,     22.000000, \
                       23.000000,     24.000000,     25.000000,     27.000000, \
                       29.000000,     31.000000,     33.000000,     35.000000, \
                       37.000000,     40.000000])

    ch4m   = np.array([  1.70000e-06,   1.70000e-06,   1.70000e-06,   1.70000e-06, \
                         1.70000e-06,   1.70000e-06,   1.70000e-06,   1.70000e-06, \
                         1.70000e-06,   1.70000e-06,   1.70000e-06,   1.70000e-06, \
                         1.69900e-06,   1.69700e-06,   1.69300e-06,   1.68500e-06, \
                         1.67485e-06,   1.66200e-06,   1.64753e-06,   1.62915e-06, \
                         1.60500e-06,   1.58531e-06,   1.55875e-06,   1.52100e-06, \
                         1.48145e-06,   1.42400e-06,   1.38858e-06,   1.34258e-06, \
                         1.28041e-06,   1.19173e-06,   1.05500e-06,   1.02223e-06, \
                         9.63919e-07,   9.04935e-07,   8.82387e-07,   8.48513e-07, \
                         7.91919e-07,   0.000000000])

    ch4mix = np.interp(h, ch4h, ch4m)
    return ch4mix
    #}}}

def BAROPOL(p_inp, h_inp, t_inp, hn_inp, tn_inp):
    #{{{
    indices = np.argsort(h_inp)
    h = h_inp[indices]
    p = p_inp[indices]
    t = t_inp[indices]

    indices = np.argsort(hn_inp)
    hn      = hn_inp[indices]
    tn      = tn_inp[indices]

    n = p.size - 1
    a = np.zeros(n)
    z = np.zeros(n)

    for i in xrange(n):
        a[i] = 0.5 * (t[i]+t[i+1]) / (h[i]-h[i+1]) * np.log(p[i+1]/p[i])
        z[i] = 0.5 * (h[i]+h[i+1])

    z0 = np.min(z)
    z1 = np.max(z)
    hn0 = np.min(hn)
    hn1 = np.max(hn)

    if hn0 < z0:
        a = np.hstack((a[0], a))
        z = np.hstack((hn0, z))
        if z0 - hn0 > 2.0:
            print('Standard atmosphere not sufficient (lower boundary).')

    if hn1 > z1:
        a = np.hstack((a, z[n-1]))
        z = np.hstack((z, hn1))
        if hn1-z1 > 10.0:
            print('Standard atmosphere not sufficient (upper boundary).')

    an = np.interp(hn, z, a)

    nn = hn.size
    pn = np.zeros(nn)
    for i in xrange(nn):
        hi = np.argmin(np.abs(hn[i]-h))
        pn[i] = p[hi]*np.exp(-an[i]*(hn[i]-h[hi])/tn[i])

    dp = np.zeros(nn-1)
    pl = np.zeros(nn-1)
    zl = np.zeros(nn-1)

    for i in xrange(nn-1):
        dp[i] = pn[i] - pn[i+1]
        pl[i] = 0.5 * (pn[i]+pn[i+1])
        zl[i] = 0.5 * (hn[i]+hn[i+1])

    for i in xrange(nn-1):
        indices = (zl >= h[i]) & (zl < h[i+1])
        ind = np.where(indices==True)[0]
        ni  = indices.sum()
        if ni >= 2:
            dpm = dp[ind].sum()

            i0 = np.min(ind)
            i1 = np.max(ind)

            x1 = pl[i0]
            x2 = pl[i1]
            y1 = dp[i0]
            y2 = dp[i1]

            bb = (y2-y1) / (x2-x1)
            aa = y1 - bb*x1
            rescale = dpm / (aa+bb*pl[indices]).sum()

            if np.abs(rescale-1.0) > 0.1:
                print('------------------------------------------------------------------------------')
                print('Warning: pressure smoothing failed at ', h[i], '...', h[i+1])
                print('rescale=', rescale)
                print('------------------------------------------------------------------------------')
            else:
                dp[indices] = rescale*(aa+bb*pl[indices])

    for i in xrange(nn-1):
        pn[i+1] = pn[i] - dp[i]

    baropol = pn

    return baropol
    #}}}

def BODHAINE(wv0, pz1, pz2):
    #{{{
    """
    wv0 = wavelength (in microns) --- can be an array
    pz1 = Pressure of lower layer (hPa)
    pz2 = Pressure of upper layer (hPa; pz1 > pz2)

    example: calculate Rayleigh optical depth between 37 km (~4 hPa) and sea level (1000 hPa) at 0.5 microns:
    in Python program:
        result=bodhaine(0.5,1000,4)

    Note: If you input an array of wavelengths, the result will also be an
          array corresponding to the Rayleigh optical depth at these wavelengths.
    """

    num = 1.0455996 - 341.29061*wv0**(-2.0) - 0.90230850*wv0**2.0
    den = 1.0 + 0.0027059889*wv0**(-2.0) - 85.968563*wv0**2.0
    tauray = 0.00210966*(num/den)*(pz1-pz2)/1013.25
    return tauray
    #}}}

def SOLFAC(doy):
    #{{{
    eps = 0.01673
    perh= 2.0
    rsun = (1.0-np.exp(np.cos(2.0*np.pi*(doy-perh)/365.0)))
    solfac = 1.0/(rsun**2)
    return solfac
    #}}}

def WEIGHTS():
    #{{{
    w = np.array([\
            0.1527534276, 0.1491729617, 0.1420961469, 0.1316886544, \
            0.1181945205, 0.1019300893, 0.0832767040, 0.0626720116, \
            0.0424925000, 0.0046269894, 0.0038279891, 0.0030260086, \
            0.0022199750, 0.0014140010, 0.0005330000, 0.000075])
    print(w)
    #}}}

if __name__ == '__main__':
    #WEIGHTS()
    result=BODHAINE(0.5,1000,4)
    print(result)
