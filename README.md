## mview

--------

Tested on macOS v10.12.3 with

- Python v3.6.0 :: Anaconda 4.3.1 (x86_64)
  - os
  - sys
  - numpy
  - matplotlib

--------

### What is this

A program that reads in and plot the binary output from MCARaTS.

General idea:
1. read in the variable information from GrADS `ctl` file (by function `MCARaTS_READ_CTL`);
2. use the variable information to contruct array from binary file (by function `MCARaTS_READ_OUTPUT`);
3. plot the array (by function `MCARaTS_PLOT_OUTPUT`).

--------

### How to install

Put this code under a directory that's in `$PATH`, e.g., `/usr/bin`.

--------

### How to use
E.g., the full path of a MCARaTS output is `/model/mcarats/test/output.bin`.

- In a terminal, run

  ```bash
  mview /model/mcarats/test/output.bin
  ```

- In a terminal, run

  ```bash
  mview
  ```

  then type in `/model/mcarats/test/output.bin`
